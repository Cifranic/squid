# squid

Squid is a caching proxy for the Web supporting HTTP, HTTPS, FTP, and more. It reduces bandwidth and improves response times by caching and reusing frequently-requested web pages. This deployment leverages docker to ensure speedy implementation. 


### 1. Pull the repo and enter it:
```terminal
git clone https://gitlab.com/Cifranic/squid.git && cd squid
```

### 2. Build the image: 
```terminal
sudo docker build -t squid_proxy .
```

### 3. Run the container:
```terminal
sudo 
```